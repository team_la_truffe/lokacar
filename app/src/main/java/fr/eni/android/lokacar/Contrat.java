package fr.eni.android.lokacar;

import java.util.Date;

/**
 * Created by ASUS on 13/06/2017.
 */

public class Contrat {

    private int id_Contrat;
    private Date date_debut;
    private Date date_fin;
    private int km_debut;
    private int km_fin;
    private double reglement;
    private String etat_depart;
    private String etat_retour;
    private int id_Gerant;

    public Contrat(int id_Contrat, Date date_debut, Date date_fin, int km_debut, int km_fin, double reglement, String etat_depart, String etat_retour, int id_Gerant) {
        this.id_Contrat = id_Contrat;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.km_debut = km_debut;
        this.km_fin = km_fin;
        this.reglement = reglement;
        this.etat_depart = etat_depart;
        this.etat_retour = etat_retour;
        this.id_Gerant = id_Gerant;
    }

    public int getId_Contrat() {
        return id_Contrat;
    }

    public void setId_Contrat(int id_Contrat) {
        this.id_Contrat = id_Contrat;
    }

    public Date getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(Date date_debut) {
        this.date_debut = date_debut;
    }

    public Date getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(Date date_fin) {
        this.date_fin = date_fin;
    }

    public int getKm_debut() {
        return km_debut;
    }

    public void setKm_debut(int km_debut) {
        this.km_debut = km_debut;
    }

    public int getKm_fin() {
        return km_fin;
    }

    public void setKm_fin(int km_fin) {
        this.km_fin = km_fin;
    }

    public double getReglement() {
        return reglement;
    }

    public void setReglement(double reglement) {
        this.reglement = reglement;
    }

    public String getEtat_depart() {
        return etat_depart;
    }

    public void setEtat_depart(String etat_depart) {
        this.etat_depart = etat_depart;
    }

    public String getEtat_retour() {
        return etat_retour;
    }

    public void setEtat_retour(String etat_retour) {
        this.etat_retour = etat_retour;
    }

    public int getId_Gerant() {
        return id_Gerant;
    }

    public void setId_Gerant(int id_Gerant) {
        this.id_Gerant = id_Gerant;
    }
}
