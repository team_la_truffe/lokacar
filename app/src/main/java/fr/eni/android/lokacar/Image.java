package fr.eni.android.lokacar;

/**
 * Created by ASUS on 13/06/2017.
 */

public class Image {

    private int id_Image;
    private int id_Voiture;
    private int id_Contrat;
    private boolean etat_depart;
    private boolean etat_retour;
    private String chemin;

    public Image(int id_Image, int id_Voiture, int id_Contrat, boolean etat_depart, boolean etat_retour, String chemin) {
        this.id_Image = id_Image;
        this.id_Voiture = id_Voiture;
        this.id_Contrat = id_Contrat;
        this.etat_depart = etat_depart;
        this.etat_retour = etat_retour;
        this.chemin = chemin;
    }

    public int getId_Image() {
        return id_Image;
    }

    public void setId_Image(int id_Image) {
        this.id_Image = id_Image;
    }

    public int getId_Voiture() {
        return id_Voiture;
    }

    public void setId_Voiture(int id_Voiture) {
        this.id_Voiture = id_Voiture;
    }

    public int getId_Contrat() {
        return id_Contrat;
    }

    public void setId_Contrat(int id_Contrat) {
        this.id_Contrat = id_Contrat;
    }

    public boolean isEtat_depart() {
        return etat_depart;
    }

    public void setEtat_depart(boolean etat_depart) {
        this.etat_depart = etat_depart;
    }

    public boolean isEtat_retour() {
        return etat_retour;
    }

    public void setEtat_retour(boolean etat_retour) {
        this.etat_retour = etat_retour;
    }

    public String getChemin() {
        return chemin;
    }

    public void setChemin(String chemin) {
        this.chemin = chemin;
    }
}
