package fr.eni.android.lokacar;

/**
 * Created by ASUS on 13/06/2017.
 */

public class Voiture {

    private int id_Voiture;
    private String marque;
    private String modele;
    private boolean urbain;
    private String type;
    private String energie;
    private int kilometrage;
    private String immatriculation;
    private double prix_jour;
    private double prix_km;
    private boolean disponible;

    public Voiture(int id_Voiture, String marque, String modele, boolean urbain, String type, String energie, int kilometrage, String immatriculation, double prix_jour, double prix_km, boolean disponible) {
        this.id_Voiture = id_Voiture;
        this.marque = marque;
        this.modele = modele;
        this.urbain = urbain;
        this.type = type;
        this.energie = energie;
        this.kilometrage = kilometrage;
        this.immatriculation = immatriculation;
        this.prix_jour = prix_jour;
        this.prix_km = prix_km;
        this.disponible = disponible;
    }

    public int getId_Voiture() {
        return id_Voiture;
    }

    public void setId_Voiture(int id_Voiture) {
        this.id_Voiture = id_Voiture;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public boolean isUrbain() {
        return urbain;
    }

    public void setUrbain(boolean urbain) {
        this.urbain = urbain;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEnergie() {
        return energie;
    }

    public void setEnergie(String energie) {
        this.energie = energie;
    }

    public int getKilometrage() {
        return kilometrage;
    }

    public void setKilometrage(int kilometrage) {
        this.kilometrage = kilometrage;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public double getPrix_jour() {
        return prix_jour;
    }

    public void setPrix_jour(double prix_jour) {
        this.prix_jour = prix_jour;
    }

    public double getPrix_km() {
        return prix_km;
    }

    public void setPrix_km(double prix_km) {
        this.prix_km = prix_km;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }
}
